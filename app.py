from urllib import response
from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_mysqldb import MySQL
import json
from datetime import date, datetime
import hashlib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from cryptography.fernet import Fernet

# Hashing keys
f_key = "KciLP0uVoMG96THB1fLEL5SZEqFA41FquVzgBd1e7-U=".encode()
salt = "RGGW6ud9GF9jDRWdfaiRQbEunMIGzhpk".encode('utf-8')

mysql = MySQL()

app = Flask(__name__)
CORS(app)

app.config['MYSQL_USER'] = 'admin'
app.config['MYSQL_PASSWORD'] = 'Leeds123'
app.config['MYSQL_HOST'] = 'escooter.ce9pavqrfhlh.us-east-1.rds.amazonaws.com'
app.config['MYSQL_DB'] = 'Escooter'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

mysql.init_app(app)

# Index route
@app.route('/')
def index():
    return "<h1>Welcome to our E-Scooter API!!</h1>"


# Used to test that a connection can be made with the API and database
@app.route('/dbconntest')
def database_connection_test():
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select version()")
        version = cursor.fetchall()
        date = datetime.today().strftime('%Y-%m-%d')
        current_time = datetime.now().strftime("%H:%M:%S")
        response = jsonify(version)
        response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns 1 or 0 if an email exists in the database or not
def check_email_already_exists(email):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("SELECT count(*) AS email_exists FROM `Customer Details` WHERE `email` = '%s';" % email)
        scooter_details = cursor.fetchall()
        response = jsonify(scooter_details)
        return response.json[0]['email_exists']
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Send a personalised e-mail to a customer for creating a new account
def send_new_account_email(FirstName, SecondName, Email):
    import smtplib, ssl

    port = 465  # For SSL
    password = "Leeds123!"

    smtp_server = "smtp.gmail.com"
    sender_email = "leedsescooterteam18@gmail.com"  # Enter your address
    receiver_email = Email  # Enter receiver address

    text = """\
        Hi %s %s,
            
        You have successfully created and account with us.

        You will now be able to log onto the app.

        Stay safe,

        From Team 18.
        """ % (FirstName, SecondName)

    message = MIMEMultipart('alternative')
    message["Subject"] = "EScooter New Account"
    message["From"] = sender_email
    message["To"] = receiver_email

    plain_text = MIMEText(text, _subtype='plain', _charset='UTF-8')
    message.attach(plain_text)

    # Create a secure SSL context
    context = ssl.create_default_context()

    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message.as_string())


# Add a new customer account to the database
@app.route('/addNewAccount', methods=['POST'])
def add_new_account():
    cursor = mysql.connection.cursor()
    try:
        _json = request.json
        _firstName = _json['FirstName']
        _secondName = _json['SecondName']
        _email = _json['Email']
        _discount = _json['Discount']
        _password = _json['Password']
        if check_email_already_exists(_email) == 1:
            duplicateResponse = jsonify("Email account already in use")
            duplicateResponse.status_code = 409
            return duplicateResponse
        elif _firstName and _secondName and _email and _discount and _password and request.method == 'POST':
            passwordKey = hashlib.pbkdf2_hmac('sha256', _password.encode('utf-8'), salt, 100000)
            sql_query = "insert into `Customer Details`(`First Name`, `Second Name`, Email, Discount, Password) VALUES(%s, %s, %s, %s, %s);"

            bind_data = (_firstName, _secondName, _email, _discount, passwordKey)
            cursor.execute(sql_query, bind_data)
            mysql.connection.commit()
            send_new_account_email(_firstName, _secondName, _email)
            response = jsonify('User Account Added Successfully')
            response.status_code = 200
            return response
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns information of a customers account if their log in details are correct 
@app.route('/login', methods=['POST'])
def login():
    cursor = mysql.connection.cursor()
    try:
        _json = request.json
        _email = _json['Email']
        _password = _json['Password']

        if _email and _password and request.method == 'POST':
            cursor.execute("select * from `Customer Details` where Email = (%s);", (_email,))
            customer_details = cursor.fetchall()
            if not customer_details:  # If the email does not exist in the database
                response = jsonify("Invalid email password combination")
                response.status_code = 404
            else:
                customer_details = json.dumps(customer_details, sort_keys=True, default=str)
                response = json.loads(customer_details)
                if response is not None:
                    returned_password = response[0]['password']
                    passwordKey = str(hashlib.pbkdf2_hmac('sha256', _password.encode('utf-8'), salt, 100000))
                    if returned_password == passwordKey:
                        cursor.execute(
                            "select CustomerID, `First Name`, `Second Name`, Email, Discount from `Customer Details` where Email = (%s);",
                            (_email,))
                        customer_details_final = cursor.fetchall()
                        response = jsonify(customer_details_final)
                        response.status_code = 200
                    else:
                        response = jsonify("Invalid email password combination")
                        response.status_code = 404
            return response
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns if the log in details for an admin are correct 
@app.route('/adminLogin', methods=['POST'])
def adminLogin():
    cursor = mysql.connection.cursor()
    try:
        _json = request.json
        _email = _json['Email']
        _password = _json['Password']

        if _email and _password and request.method == 'POST':
            cursor.execute("select * from `Admin Details` where Email = (%s);", (_email,))
            customer_details = cursor.fetchall()
            if not customer_details:  # If the email does not exist in the database
                response = jsonify("Invalid email password combination")
                response.status_code = 404
            else:
                response = jsonify(customer_details)
                if response is not None:
                    returned_password = response.json[0]['Password']
                    if returned_password == _password:
                        response.status_code = 200
                    else:
                        response = jsonify("Invalid email password combination")
                        response.status_code = 404
            return response
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Get all information for a specified scooterID
@app.route('/getScooterDetails/<int:scooter_id>', methods=['GET'])
def get_scooter_details(scooter_id):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select * from `Scooter Details`  WHERE ScooterID = %d" % scooter_id)
        scooter_details = cursor.fetchall()
        if not scooter_details:
            response = jsonify("Invalid scooter ID")
            response.status_code = 404
        else:
            response = jsonify(scooter_details)
            response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns 0 or 1 if the scooter scooter is available or not
def check_scooter_availability(scooter_id):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select available from `Scooter Details` where ScooterID = %s" % scooter_id)
        scooter_details = cursor.fetchall()
        response = jsonify(scooter_details)
        return response.json[0]["available"]
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Get information for all scooters
@app.route('/getScooterDetails/all', methods=['GET'])
def get_all_scooter_details():
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select * from `Scooter Details`")
        scooter_details = cursor.fetchall()
        response = jsonify(scooter_details)
        response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Update the information for a specified scooterID
@app.route('/updateScooterDetails', methods=['PUT'])
def update_scooter_details():
    cursor = mysql.connection.cursor()
    try:
        _json = request.json
        _availability = _json['Availability']
        _currentLocation = _json['CurrentLocation']
        _scooterID = _json['ScooterID']
        if _availability and _scooterID and _currentLocation and request.method == 'PUT':
            sql_query = "UPDATE `Scooter Details` SET Available = %s, CurrentLocation = %s WHERE scooterID=%s;"
            bind_data = (_availability, _currentLocation, _scooterID)
            cursor.execute(sql_query, bind_data)
            mysql.connection.commit()
            response = jsonify('Details Updated Added Successfully')
            response.status_code = 200
            return response
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Sets a specified scooter to be at a specified location with specified availability
@app.route('/returnScooter', methods=['PUT'])
def return_scooter():
    cursor = mysql.connection.cursor()
    try:
        _json = request.json
        _availability = _json['Availability']
        _currentLocation = _json['CurrentLocation']
        _scooterID = _json['ScooterID']
        if _availability and _scooterID and _currentLocation and request.method == 'PUT':
            sql_query = "UPDATE `Scooter Details` SET Available = %s, CurrentLocation = %s WHERE scooterID=%s;"
            bind_data = (_availability, _currentLocation, _scooterID)
            cursor.execute(sql_query, bind_data)
            mysql.connection.commit()
            # availability and returned match in this case
            sql_query = "UPDATE `Session` SET Returned = %s WHERE scooterID=%s;"
            bind_data = (_availability, _scooterID)
            cursor.execute(sql_query, bind_data)
            mysql.connection.commit()
            response = jsonify('Details Updated Added Successfully')
            response.status_code = 200
            return response
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()

# Sets a specified scooterID to a specified availability
# returns 1 if successful, 0 if not.
def update_scooter_availability(_availability, _scooterID):
    cursor = mysql.connection.cursor()
    try:
        if _availability and _scooterID:
            sql_query = "UPDATE `Scooter Details` SET Available = %s WHERE scooterID=%s;"
            bind_data = (_availability, _scooterID)
            cursor.execute(sql_query, bind_data)
            mysql.connection.commit()
            return 1
        else:
            return 0
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Get all scooter pricing options 
@app.route('/getScooterPricing/all', methods=['GET'])
def get_all_pricing_details():
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select * from `Pricing`")
        scooter_details = cursor.fetchall()
        response = jsonify(scooter_details)
        response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns the current session information - if one exists - for a specified customerID 
@app.route('/getCurrentSession/<int:customer_id>', methods=['GET'])
def get_session_details(customer_id):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select * from Session Where CustomerId = %d AND Returned = 0" % customer_id)
        scooter_details = cursor.fetchall()
        # dumps is required as normal json doesn't like the "TimeHired" field that is returned
        scooter_details = json.dumps(scooter_details, sort_keys=True, default=str)
        response = jsonify(scooter_details)
        response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Extends a session for a specified customer
# Returns cost of ExtendedCostID - OriginalCostID
@app.route('/extendSession', methods=['PUT'])
def extend_session():
    cursor = mysql.connection.cursor()
    try:
        _json = request.json
        _customerId = _json['CustomerID']
        _scooterId = _json['ScooterID']
        _costId = _json['CostID']
        if _customerId and _scooterId and _costId and request.method == 'PUT':

            # Get original cost id before changing.
            cursor.execute("select CostID from Session Where CustomerID = %s and ScooterID = %s AND Returned = 0" % (
                _customerId, _scooterId))
            oringinalCostId = jsonify(cursor.fetchall()).json[0]["CostID"]

            # Get original cost price.
            cursor.execute("select Price from Pricing Where CostID = %s;" % oringinalCostId)
            oringinalPrice = jsonify(cursor.fetchone()).json["Price"]

            # Get new cost price.
            cursor.execute("select Price from Pricing Where CostID = %s;" % _costId)
            newPrice = jsonify(cursor.fetchone()).json["Price"]

            cursor.execute(
                "update Session set CostID = %s where CustomerID = %s and ScooterID = %s and Returned = 0;" % (
                    _costId, _customerId, _scooterId))
            mysql.connection.commit()
            response = jsonify(newPrice - oringinalPrice)

            return response
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Send email to a specified customerID
# Returns 1 if email was sent successfully, 0 if not
def send_email(scooter_id, customer_id, cost_id):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select `First Name`, Email from `Customer Details` Where CustomerId = %s" % customer_id)
        customerDetails = cursor.fetchone()
        name = customerDetails['First Name']
        email = customerDetails['Email']
        cursor.execute("select `Duration(mins)` from Pricing Where CostID = %s" % cost_id)
        duration = cursor.fetchone()['Duration(mins)']

        import smtplib, ssl

        port = 465  # For SSL
        password = "Leeds123!"

        smtp_server = "smtp.gmail.com"
        sender_email = "leedsescooterteam18@gmail.com"  # Enter your address
        receiver_email = email  # Enter receiver address

        text = """\
            Hi %s,
            
            You have successfully booked escooter %s for %s minutes.

            Make sure to return the scooter on the app once you are done.

            Stay safe,

            From Team 18.
            """ % (name, scooter_id, duration)

        message = MIMEMultipart('alternative')
        message["Subject"] = "EScooter Booking Confirmation"
        message["From"] = sender_email
        message["To"] = receiver_email

        plain_text = MIMEText(text, _subtype='plain', _charset='UTF-8')
        message.attach(plain_text)

        # Create a secure SSL context
        context = ssl.create_default_context()

        with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(sender_email, receiver_email, message.as_string())

        return 1
    except Exception as e:
        print(e)
        return 0
    finally:
        cursor.close()


# Add a new session for a customer, change availability of hired scooter
@app.route('/addNewSession', methods=['POST'])
def add_new_session():
    cursor = mysql.connection.cursor()
    try:
        _json = request.json
        _customerID = _json['CustomerID']
        _scooterID = _json['ScooterID']
        _costID = _json['CostID']

        # Check availability incase scooter has been booked in the time someone has taken to go through booking
        if check_scooter_availability(_scooterID) == 0:
            response = jsonify("This scooter is no longer available")
            response.status_code = 409
            return response
        elif _customerID and _customerID and _scooterID and _costID and request.method == 'POST':
            date = datetime.today().strftime('%Y-%m-%d')
            current_time = datetime.now().strftime("%H:%M:%S")

            sql_query = "insert into `Session`(`CustomerID`, `ScooterID`, DateHired, TimeHired, Paid, CostID, Returned) VALUES(%s, %s, %s, %s, %s, %s, %s);"

            bind_data = (_customerID, _scooterID, date, current_time, 1, _costID, 0)
            cursor.execute(sql_query, bind_data)
            mysql.connection.commit()
            if update_scooter_availability("0", _scooterID) == 1:
                if (send_email(_scooterID, _customerID, _costID) == 1):
                    response = jsonify('Session Created Successfully')
                    response.status_code = 200
                else:
                    response = jsonify('Session Created Successfully, but email not sent.')
                    response.status_code = 200
            else:
                response = jsonify('Session NOT Created Successfully')
                response.status_code = 400
            return response
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns the encrypted key for a specified key and message
def encrypt(message: bytes, key: bytes) -> bytes:
    return Fernet(key).encrypt(message)


# Returns the decrypted plain text for a specified key and token
def decrypt(token: bytes, key: bytes) -> bytes:
    return Fernet(key).decrypt(token)


# Add a new card to a customer's account, also hashes the card number for improved security on the database
@app.route('/addNewCardDetails', methods=['POST'])
def add_new_card():
    cursor = mysql.connection.cursor()
    try:
        _json = request.json
        _customerID = _json['CustomerID']
        _expiryDate = _json['ExpiryDate']
        _cardNumber = _json['CardNumber']
        if _customerID and _expiryDate and _cardNumber and request.method == 'POST':
            # For hashing the card number
            card_number_cipher = encrypt(_cardNumber.encode(), f_key) 
            query = "insert into `Card Details`(`CustomerID`, `Card Number`, `Expiry Date`) VALUES(%s, %s, %s);"
            bind_data = (_customerID, card_number_cipher, _expiryDate)
            cursor.execute(query, bind_data)
            mysql.connection.commit()
            response = jsonify('Card Added Successfully')
            response.status_code = 200
            return response
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns all card info for a specified customerID
@app.route('/getCardDetails/<int:customer_id>', methods=['GET'])
def get_customer_card_details(customer_id):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select * from `Card Details` Where CustomerId = %d" % customer_id)
        card_details = cursor.fetchall()
        print(card_details)
        if not card_details:
            response = jsonify("No card on account")
        else:
            card_details = json.dumps(card_details, sort_keys=True, default=str)
            card_details_raw = json.loads(card_details)
            card_detail = card_details_raw[0]
            token = card_detail['Card Number']
            card_number = decrypt(token.encode(), f_key).decode()
            card_detail['Card Number'] = card_number
            print(card_detail)
            response = jsonify(card_detail)
            response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Adds a new item of feedback
@app.route('/addNewFeedback', methods=['POST'])
def new_feedback():
    cursor = mysql.connection.cursor()
    try:
        _json = request.json
        _userId = _json['UserId']
        _feedbackContents = _json['FeedbackContents']
        if _userId and _feedbackContents and request.method == 'POST':
            sql_query = "insert into `Feedback`(`User ID`, `Feedback Contents`, Priority) VALUES(%s, %s, 3);"
            bind_data = (_userId, _feedbackContents)
            cursor.execute(sql_query, bind_data)
            mysql.connection.commit()
            response = jsonify('Feedback Added Successfully')
            response.status_code = 200
            return response
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns all feedback on database
@app.route('/getFeedback/all', methods=['GET'])
def getallfeedback():
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select * from `Feedback`;")
        feedback_list = cursor.fetchall()
        response = jsonify(feedback_list)
        response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Return info on a specified session
@app.route('/getSession/<int:id>', methods=['GET'])
def get_Session(id):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select TimeHired, DateHired, FinalCost, Paid, CostID, Returned, CustomerID, SessionID from `Session` where SessionID = %d;" % id)
        session = cursor.fetchone()
        if not session:
            response = jsonify("Data not found")
            response.status_code = 404
        else:
            session['TimeHired'] = str(session['TimeHired'])
            session['DateHired'] = str(session['DateHired'])
            response = jsonify(str(session))
            response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns all session that have been returned or not
@app.route('/getCurrentSessions', methods=['GET'])
def get_current_session():
    cursor = mysql.connection.cursor()
    d = 0
    try:
        cursor.execute("select SessionID, ScooterID, CostID from `Session` where Returned = %d;" %d)
        session_details = cursor.fetchall()
        for a in session_details:
            location = get_location_from_scooterID(a["ScooterID"])
            scooterLocation = {"ScooterLocation":location}
            a["ScooterLocation"] = location
        response=jsonify(session_details)
        response.status_code=200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Allows an employee to extend of session for a specified scooterID to a specified CostID
@app.route('/employeeExtendSession', methods=['PUT'])
def employee_extend_session():
    cursor = mysql.connection.cursor()
    try:
        _json = request.json
        _scooterId = _json['ScooterID']
        _costId = _json['CostID']
        if _scooterId and _costId and request.method == 'PUT':

            # Get original cost id before changing.
            cursor.execute("select CostID from Session Where ScooterID = %s AND Returned = 0" % (
                 _scooterId))
            oringinalCostId = jsonify(cursor.fetchall()).json[0]["CostID"]

            # Get original cost price.
            cursor.execute("select Price from Pricing Where CostID = %s;" % oringinalCostId)
            oringinalPrice = jsonify(cursor.fetchone()).json["Price"]

            # Get new cost price.
            cursor.execute("select Price from Pricing Where CostID = %s;" % _costId)
            newPrice = jsonify(cursor.fetchone()).json["Price"]

            cursor.execute(
                "update Session set CostID = %s where ScooterID = %s and Returned = 0;" % (
                    _costId, _scooterId))
            mysql.connection.commit()
            response = jsonify(newPrice - oringinalPrice)
            return response
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns the current locationID of a specified scooterID       
def get_location_from_scooterID(ID):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select CurrentLocation from `Scooter Details` where ScooterID = %d " %ID)
        session_details = cursor.fetchone()
        response = (session_details['CurrentLocation'])
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns the amount of time a specified customer has left on their booking
@app.route('/getTimeLeft/<int:customer_id>', methods=['GET'])
def get_time_left(customer_id):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute(
            "select `Duration(mins)`,DateHired, TimeHired from Pricing, Session Where CustomerId = %d AND Returned = 0 and Session.CostID=Pricing.CostID" % customer_id)
        rawDetails = cursor.fetchone()
        detailsDump = json.dumps(rawDetails, default=str)
        jsonobj = json.loads(detailsDump)
        datetimeBookedStr = jsonobj['DateHired'] + " " + jsonobj['TimeHired']
        datetimeBookedObj = datetime.strptime(datetimeBookedStr, '%Y-%m-%d %H:%M:%S')
        currentTimeObj = datetime.today().replace(microsecond=0)
        timeDiff = currentTimeObj - datetimeBookedObj
        print(datetimeBookedObj)
        print(currentTimeObj)
        print(timeDiff)
        print(detailsDump)
        response = jsonify(str(timeDiff))
        response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns total costs for a specified date
@app.route('/getDailyTotals/<string:date>', methods=['GET'])
def get_daily_totals(date):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select SUM(FinalCost) from `Session` where DateHired = %s " % date)
        daily_totals = cursor.fetchall()
        if not daily_totals:
            response = jsonify("Data not found")
            response.status_code = 404
        else:
            response = jsonify(daily_totals)
            response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns rental totals between two specified dates for a specified CostID
@app.route('/getRentalTotals/<string:Sdate>/<string:Edate>/<int:id>', methods=['GET'])
def get_rental_totals(Sdate, Edate, id):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select sum(FinalCost) from `Session` where CostID = %d and DateHired between %s and %s;" % (
            id, Sdate, Edate))
        daily_rental = cursor.fetchall()
        print(daily_rental)
        if not daily_rental:
            response = jsonify("Data not found")
            response.status_code = 404
        else:
            response = jsonify(daily_rental)
            response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Resolves the ticket for a specified feeedbackID
@app.route('/resolveFeedback/<int:id>', methods=['PUT'])
def resolve_Feedback(id):
    cursor = mysql.connection.cursor()
    try:
        if request.method == 'PUT':
            cursor.execute("update `Feedback` set `Priority` = 0 where `Feedback ID` = (%d);" % (id))
            mysql.connection.commit()
            response = jsonify('Scooter availability updated successfully')
            response.status_code = 200
            return response

    except Exception as e:
        print(e)

    finally:
        cursor.close()


# Returns address of specified locationID
@app.route('/getAddress/<int:id>', methods=['GET'])
def get_Address(id):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select Address from `Scooter Location` where LocationID = %d;" % id)
        address = cursor.fetchall()
        if not address:
            response = jsonify("Data not found")
            response.status_code = 404
        else:
            response = jsonify(address)
            response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Chanege the prioity of a specified PriorityID
@app.route('/changeFeedbackPriority/<int:id>/<int:newPriority>', methods=['PUT'])
def change_feedback_priority(id, newPriority):
    cursor = mysql.connection.cursor()
    try:
        if request.method == 'PUT':
            cursor.execute("update `Feedback` set `Priority` = (%d) where `Feedback ID` = (%d);" % (newPriority, id))
            mysql.connection.commit()
            response = jsonify('Scooter availability updated successfully')
            response.status_code = 200
            return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Change price of a specified CostID
@app.route('/changePrice/<int:id>/<int:newPrice>', methods=['PUT'])
def change_prices(id, newPrice):
    cursor = mysql.connection.cursor()
    try:
        if request.method == 'PUT':
            cursor.execute("update `Pricing` set `Price` = (%d) where `CostID` = (%d);" % (newPrice, id))
            mysql.connection.commit()
            response = jsonify('Price updated successfully')
            response.status_code = 200
            return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()


# Returns 1 if they are eligible for a discount, 0 if they are not.
@app.route('/getDiscountEligibility/<string:customer_id>', methods=['GET'])
def get_discount_eligibility(customer_id):
    cursor = mysql.connection.cursor()
    try:
        cursor.execute("select Discount from `Customer Details` where CustomerID = %s;" % customer_id)
        discountRaw = cursor.fetchone()
        discount = jsonify(discountRaw)
        discount = discount.json["Discount"]
        if (discount == 1):
            response = jsonify(1)
            response.status_code = 200
        else:
            cursor.execute(
                "select sum(`Duration(mins)`) as TotalMins from Session, Pricing where CustomerID = %s and Session.CostID = Pricing.CostID and DateHired between date_sub(now(),INTERVAL 1 WEEK) and now();" % customer_id)
            total = cursor.fetchone()
            print(total)

            discountHoursNeeded = 8
            if (total["TotalMins"] is not None and discountHoursNeeded * 60 <= total["TotalMins"]):
                isEligible = 1
            else:
                isEligible = 0

            response = jsonify(isEligible)
            response.status_code = 200
        return response
    except Exception as e:
        print(e)
    finally:
        cursor.close()

# Used if an unknown route is used.
@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Record not found: ' + request.url,
    }
    response = jsonify(message)
    response.status_code = 404
    return response

