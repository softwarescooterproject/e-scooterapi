# E-ScooterApi

API can be accessed at https://e-scooter-api.herokuapp.com/

We could have not developed an Api for this project and instead accessed the database (DB) directly from our mobile and web apps. However this would have been bad practice for the following reasons:

    1. Poor scalability - if the apps were to become popular, the DB could not support thousands of unique connections.
    2. Terrible security - to connect directly to a DB from the apps would require storing the DB credentials on them. This would expose the DB to malicious activity such as it being eddited by users for free trips, card details being leaked or the whole DB being wiped.
    3. Larger code duplication - the mobile and web apps often require the same information off of the DB. Without an API, the queries have to be written to be platform specific.

# How to run project for local testing - Using python3.7:
	-	run "git clone https://gitlab.com/softwarescooterproject/e-scooterapi.git"
	-	run "cd e-scooterapi"
	-	run "virtualenv venv"
	-	run ".\venv\Scripts\activate" (windows) or "source venv/bin/activate" (linux)
	-	Note: If using uni system, also run "module add anaconda3"
	-	run "pip install -r requirements.txt" or on uni systems run "pip3 install -r requirements.txt"
	-	run "flask run"
	-	Open browser and go to the address "localhost:5000"
	-	Only make changes to the app.py file

# How to upload changes to heroku:
	
	-	Clone the git repo	
	-	Make desired changes	
	-	run "git add app.py"	
	-	run "git commit -m "<What you've added>"	
	-	run "heroku login" and login from you browser
	-	run "heroku git:remote -a e-scooter-api"	
	-	run "git push origin main"	
	-	run "git push heroku main"
	-	Update API documentation as necessary at https://leeds365-my.sharepoint.com/:w:/g/personal/sc20om_leeds_ac_uk/EY3WGCk1BEtPjN0hg4ixX_4BuKP9058FgKlaCcEZXnv2eg?e=KhTu9Y

Note: Above instructions are with the assumption that you have git and heroku CLI installed. Heroku CLI can be installed at https://devcenter.heroku.com/articles/heroku-cli
Additionally, if you don't have a heroku account, make one using your uni emails as with them you will already be a collaborator on the project.
